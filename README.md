# Einführung

## Setup

VSCode installieren - https://code.visualstudio.com/
VSCode Extensions installieren - Live Server - Alpine.js IntelliSense
Alpine CDN

```html
<script defer src="https://cdn.jsdelivr.net/npm/alpinejs@3.x.x/dist/cdn.min.js"></script>
```

Tailwind CDN

```html
<script src="https://cdn.tailwindcss.com"></script>
```

Alpine testen

```html
<div x-data="{ message: 'hallo' }" x-text="message"></div>
```

# Inhalte

## x-data (1)

Komponente und Eigenschaften definieren

```html
<!-- Komponente definieren -->
<div x-data></div>

<!-- Komponente mit string, int, boolean, object, arrays -->
<div x-data="{ name: 'John', age: 23, status: true, user: { name: 'Jihn', age:22 }, colors: ['Rot', 'Blau'] }"></div>
```

## x-text

Text vom Element setzen

```html
<!-- Text vom Element setzen -->
<div class="p-4" x-data="{ name: 'John' }">
  <span class="font-semibold">Name: </span>
  <span x-text="name"></span>
</div>
```

## x-on (1)

EventListener für ein Element hinzufügen

```html
<!-- Klick Event hinzufügen > Kurzschreibweise -->
<div class="p-4" x-data="{ message: 'Hallo Welt' }">
  <button class="bg-slate-500 px-4 py-2 text-white" @click="message = 'Hallo andere Welt'">Text ändern</button>
  <div x-text="message"></div>
</div>
```

## x-show

Sichtbarkeit für ein Element steuern

```html
<!-- Sichtbarkeit für ein Element steuern -->
<div class="p-4" x-data="{ open: false }">
  <button class="bg-slate-500 px-4 py-2 text-white" @click="open = true">Anzeigen</button>
  <div x-show="open">
    Lorem ipsum dolor sit amet consectetur adipisicing elit. Est sint error iste temporibus! Molestiae tempora tempore
    libero odit sunt, maxime explicabo ducimus, ipsa quibusdam ad sit cumque? Saepe, repudiandae dolores eaque optio ab
    quos molestiae amet est, nam aliquam maiores?
  </div>
</div>
```

## x-cloak

Element ausblenden, bis Alpine fertig initialisiert ist

```html
<style>
  [x-cloak] {
    display: none !important;
  }
</style>

<!-- Element ausblenden, bis Alpine fertig initialisiert ist -->
<div class="p-4" x-data>
  <div x-show="false" x-cloak="">
    Lorem ipsum dolor sit amet consectetur adipisicing elit. Quam assumenda maiores eum nisi delectus dolorem tempore,
    temporibus veritatis error dolores voluptatum at nostrum sed quas, nobis in soluta dolore veniam.
  </div>
</div>
```

## x-if

Ins DOM einspielen, wenn die Bedingung erfüllt ist

```html
<!-- Ins DOM einspielen, wenn die Bedingung erfüllt ist -->
<div class="p-4" x-data="{ open: false }">
  <button class="bg-slate-500 px-4 py-2 text-white" @click="open = true">Anzeigen</button>
  <template x-if="open">
    <div>
      Lorem ipsum dolor sit amet consectetur adipisicing elit. Nihil velit quod, quibusdam placeat ipsum dignissimos
      eius temporibus adipisci nesciunt ratione in! Sapiente, voluptatem similique necessitatibus earum molestiae neque
      quo, quas fugit unde illum nobis omnis sequi ullam commodi iure hic, enim eum? Tenetur vel voluptatum magni autem
      itaque, illo assumenda.
    </div>
  </template>
</div>
```

## x-bind (1)

Attributte ändern oder hinzufügen

```html
<!-- Attributte ändern oder hinzufügen > Kurzschreibweise -->
<div class="p-4" x-data="{ color: 'bg-slate-500', img: 'https://via.placeholder.com/500'}">
  <button class="px-4 py-2 text-white" :class="color" @click="color = 'bg-green-500'">Farbe ändern</button>
  <button class="bg-slate-500 px-4 py-2 text-white" @click="img = 'https://via.placeholder.com/300'">
    Bild tauschen
  </button>
  <img :src="img" alt="Placeholder" />
</div>
```

## x-bind (2)

Abhängigkeiten für bind

```html
<div class="space-y-4 p-4" x-data="{ open: false }">
  <button class="bg-slate-500 px-4 py-2 text-white" x-on:click="open = ! open">Toggle</button>
  <span x-text="open"></span>

  <!-- If-Else Abhängigkeit -->
  <div class="p-4" :class="open ? 'bg-green-500' : 'bg-red-500'">If-Else Abhängigkeit</div>

  <!-- if true Abhängigkeit -->
  <div class="p-4" :class="open && 'bg-green-500'">if true Abhängigkeit</div>

  <!-- if false Abhängigkeit -->
  <div class="p-4" :class="open || 'bg-red-500'">if false Abhängigkeit</div>
</div>
```

## x-on (2)

Weitere Maus Events

```html
<div class="p-4 space-y-4">
  <!-- Klick und Doppelklick Event -->
  <div class="p-4" x-data="{ open: false }">
    <button class="bg-slate-500 px-4 py-2 text-white" @click="text = true" @dblclick="text = false">Anzeigen</button>
    <div class="p-4" x-show="open">
      Lorem ipsum dolor sit amet consectetur adipisicing elit. Est sint error iste temporibus! Molestiae tempora tempore
      libero odit sunt, maxime explicabo ducimus, ipsa quibusdam ad sit cumque? Saepe, repudiandae dolores eaque optio
      ab quos molestiae amet est, nam aliquam maiores?
    </div>
  </div>
  <!-- Element betretten und verlassen Event -->
  <div class="p-4" x-data="{ color: 'bg-slate-200' }">
    <div class="p-4" :class="color" @mouseenter="color = 'bg-green-200'" @mouseleave="color = 'bg-slate-200'">
      Lorem ipsum dolor sit amet consectetur adipisicing elit. Est sint error iste temporibus! Molestiae tempora tempore
      libero odit sunt, maxime explicabo ducimus, ipsa quibusdam ad sit cumque? Saepe, repudiandae dolores eaque optio
      ab quos molestiae amet est, nam aliquam maiores?
    </div>
  </div>
  <!-- Maus gedrückt und Maus losgelassen Event -->
  <div class="p-4" x-data="{ size: 'scale-100' }" @mouseup="size = 'scale-100'" @mousedown="size = 'scale-105'">
    <div class="bg-slate-100 p-4" :class="size">
      Lorem ipsum dolor sit amet consectetur adipisicing elit. Est sint error iste temporibus! Molestiae tempora tempore
      libero odit sunt, maxime explicabo ducimus, ipsa quibusdam ad sit cumque? Saepe, repudiandae dolores eaque optio
      ab quos molestiae amet est, nam aliquam maiores?
    </div>
  </div>
</div>
```

## x-model (1)

Input an Komponentendaten binden

```html
<div class="p-4 space-y-4">
  <!-- Textinput an Komponentendaten binden -->
  <div class="p-4" x-data="{ input: '' }">
    <div class="flex items-center gap-4">
      <input class="w-1/2 border border-slate-500 p-2" type="text" placeholder="Textinput" x-model="input" />
      <div x-text="input"></div>
    </div>
  </div>
  <!-- Textarea an Komponentendaten binden -->
  <div class="p-4" x-data="{ textarea: '' }">
    <div class="flex items-center gap-4">
      <textarea
        class="w-1/2 border border-slate-500 p-2"
        type="text"
        placeholder="Textarea"
        x-model="textarea"
      ></textarea>
      <div x-text="textarea"></div>
    </div>
  </div>
</div>
```

## x-html

HTML Inhalt setzen

```html
<!-- HTML Inhalt setzen -->
<div class="p-4" x-data="{ message: 'Lorem ipsum dolor <br/> sit amet consectetur, adipisicing elit.' }">
  <span x-html="message"></span>
</div>
```

## x-model (2)

Checkbox an Komponentendaten binden

```html
<!-- Checkbox an Komponentendaten binden -->
<div class="space-y-4 p-4" x-data="{ checkbox: [] }">
  <div class="flex min-h-10 items-center gap-4">
    <input class="border border-slate-500 p-2" type="checkbox" placeholder="Textinput" value="1" x-model="checkbox" />
    <input class="border border-slate-500 p-2" type="checkbox" placeholder="Textinput" value="2" x-model="checkbox" />
    <input class="border border-slate-500 p-2" type="checkbox" placeholder="Textinput" value="3" x-model="checkbox" />
    <span x-text="checkbox"></span>
  </div>
</div>
```

## x-model (3)

Radio an Komponentendaten binden

```html
<!-- Radio an Komponentendaten binden -->
<div class="space-y-4 p-4" x-data="{ radio: '' }">
  <div class="flex min-h-10 items-center gap-4">
    <input class="border border-slate-500 p-2" type="radio" placeholder="Textinput" value="true" x-model="radio" />
    <input class="border border-slate-500 p-2" type="radio" placeholder="Textinput" value="false" x-model="radio" />
    <span x-text="radio"></span>
  </div>
</div>
```

## x-model (4)

Range an Komponentendaten binden

```html
<!-- Range an Komponentendaten binden -->
<div class="p-4" x-data="{ range: 0 }">
  <div class="flex items-center gap-4">
    <input
      class="w-1/2 border border-slate-500 p-2"
      type="range"
      placeholder="Textinput"
      min="0"
      step="10"
      max="100"
      x-model="range"
    />
    <span x-text="range"></span>
  </div>
</div>
```

## x-model (5)

Select an Komponentendaten binden

```html
<!-- Select an Komponentendaten binden -->
<div class="p-4" x-data="{ select: '' }">
  <div class="flex items-center gap-4">
    <select class="w-1/2 border border-slate-500 p-2" placeholder="Textinput" x-model="select">
      <option value="" disabled>Auswahl</option>
      <option>Rot</option>
      <option>Blau</option>
    </select>
    <span x-text="select"></span>
  </div>
</div>
```

## x-for

For Schleife definieren

```html
<div class="space-y-4 p-4">
  <!-- Einfache Schleife -->
  <div class="p-4 text-center" x-data="{ colors: ['Rot', 'Blau', 'Grün'] }">
    <template x-for="color in colors">
      <div class="" x-text="color"></div>
    </template>
  </div>
  <!-- Schleife mit Index -->
  <div class="p-4 text-center" x-data="{ colors: ['Rot', 'Blau', 'Grün'] }">
    <template x-for="(color, index) in colors">
      <div class="">
        <span x-text="index"></span>
        <span x-text="color"></span>
      </div>
    </template>
  </div>
  <!-- Schleife mit Ids -->
  <div
    class="p-4 text-center"
    x-data="{ colors: [{ id: 12, label: 'Rot' }, { id: 23, label: 'Blau' }, { id: 1, label: 'Grün' }] }"
  >
    <template x-for="color in colors">
      <div class="">
        <span x-text="color.id"></span>
        <span x-text="color.label"></span>
      </div>
    </template>
  </div>
  <!-- Feste Schleife  -->
  <div class="space-y-4 p-4 text-center" x-data>
    <template x-for="i in 10">
      <div class="" x-text="i"></div>
    </template>
  </div>
</div>
```

## x-model (6)

Verzögerung der Inputübergabe mit debounce

```html
<div class="space-y-4 p-4">
  <!-- Verzögerung der Inputübergabe mit debounce -->
  <div class="flex items-center gap-4" x-data="{ input: '' }">
    <input class="w-1/2 border border-slate-500 p-2" type="text" placeholder="Textinput" x-model.debounce="input" />
    <span x-text="input"></span>
  </div>
  <!-- Mit angepasster Zeit -->
  <div class="flex items-center gap-4" x-data="{ input: '' }">
    <input
      class="w-1/2 border border-slate-500 p-2"
      type="text"
      placeholder="Textinput"
      x-model.debounce.1000ms="input"
    />
    <span x-text="input"></span>
  </div>
  <!-- Inputübergabe beim Fokuswechsel -->
  <div class="flex items-center gap-4" x-data="{ input: '' }">
    <input class="w-1/2 border border-slate-500 p-2" type="text" placeholder="Textinput" x-model.lazy="input" />
    <span x-text="input"></span>
  </div>
</div>
```

## x-on (3)

Tastatur Events

```html
<body :class="color" x-data="{ color: '' }" @keydown.enter="color = 'bg-green-400'" @keyup.enter="color = ''"></body>
```

Subkeys

```html
<div class="p-4" x-data="{ color: '' }">
  <button
    class="bg-slate-500 px-4 py-2 text-white"
    @click="color = 'bg-slate-500'"
    @click.shift="color = 'bg-red-500'"
    @click.ctrl="color = 'bg-green-500'"
    @click.alt="color = 'bg-yellow-500'"
    :class="color"
  >
    Button
  </button>
</div>
```

## x-on (4)

Prevent methode

```html
<!-- Prevent methode -->
<div x-data>
  <div class="h-[2000px] bg-gray-400"></div>
  <form class="p-4" action="" @submit.prevent>
    <button class="w-full bg-slate-500 px-4 py-2 text-white">Submit</button>
  </form>
</div>
```

Outside methode

```html
<!-- Outside methode -->
<div class="bg-slate-100 p-4" x-data="{ open: false }" @click.outside="open = false">
  <button class="bg-slate-500 px-4 py-2 text-white" @click="open = true">Anzeigen</button>
  <div x-show="open">
    Lorem ipsum dolor sit amet consectetur adipisicing elit. Est sint error iste temporibus! Molestiae tempora tempore
    libero odit sunt, maxime explicabo ducimus, ipsa quibusdam ad sit cumque? Saepe, repudiandae dolores eaque optio ab
    quos molestiae amet est, nam aliquam maiores?
  </div>
</div>
```

Window methode

```html
<!-- Window methode -->
<div
  class="h-screen"
  :class="color"
  x-data="{ color: '' }"
  @keydown.enter.window="color = 'bg-green-400'"
  @keyup.enter.window="color = ''"
></div>
```

## x-transition

Übergänge mit Tailwind

```html
<div class="space-y-4 p-4" x-data="{ open: true }">
  <button class="bg-slate-500 px-4 py-2 text-white" @click="open = ! open">Toggle</button>
  <!-- 
        enter: Klassen für die gesamte Einblendung
        enter-start: Klassen für den ersten Frame der Einblendung
        enter-end: Klassen ab dem ende vom ersten Frame und bis zum Ende der Einblendung 
        leave: Klassen für die gesamte Ausblendung
        leave-start: Klassen für den ersten Frame der Ausblendung
        leave-end: Klassen ab dem ende vom ersten Frame und bis zum Ende der Ausblendung 
    -->
  <div
    class="border border-slate-500 p-4"
    x-show="open"
    x-transition:enter="transition ease-out duration-500"
    x-transition:enter-start="scale-50"
    x-transition:enter-end="scale-100"
    x-transition:leave="transition ease-in duration-500"
    x-transition:leave-start="scale-100"
    x-transition:leave-end="scale-50"
  >
    Lorem ipsum dolor, sit amet consectetur adipisicing elit. Maxime consequuntur non perferendis cupiditate! Dolorum
    laudantium voluptate dolores quisquam laboriosam, cumque architecto nisi ullam nemo veniam aliquam aut perferendis
    explicabo qui eveniet officiis quam pariatur animi modi ut amet sit. Expedita alias quidem ipsam quasi, doloremque
    assumenda et illum cumque. Non.
  </div>
</div>
```

## x-teleport

Element im DOM verschieben

```html
<!-- Element im DOM verschieben -->
<div class="p-4" x-data="{ open: false }">
  <div class="bg-blue-400 p-4">
    <div class="bg-green-400 p-4">
      <div id="dummy"></div>
    </div>
  </div>
</div>
<template x-data x-teleport="#dummy">
  <div class="bg-red-400 p-4"></div>
</template>
```

## $dispatch

Benachrichtigung senden

```html
<!-- Benachrichtigung senden -->
<div class="p-4" x-data="{ message: '' }" @message="message = 'Hallo Welt'">
  <button class="bg-slate-500 px-4 py-2 text-white" @click="$dispatch('message')">Benachrichtigen</button>
  <div x-text="message"></div>
</div>
```

Benachrichtigung mit Parametern

```html
<!-- Benachrichtigung senden mit Parametern -->
<div class="p-4" x-data="{ message: '' }" @message="message = $event.detail.text">
  <button class="bg-slate-500 px-4 py-2 text-white" @click="$dispatch('message', { text: 'Hallo Parameter' })">
    Benachrichtigen mit Parameter
  </button>
  <div x-text="message"></div>
</div>
```

Andere Komponenten benachrichtigen

```html
<!-- Andere Komponenten benachrichtigen -->
<div class="p-4" x-data="{ message: '' }" @global.window="message = $event.detail.text">
  <button class="bg-slate-500 px-4 py-2 text-white" @click="message = 'Hallo hier'">Andere Komponente</button>
  <div x-text="message"></div>
</div>
<div class="p-4" x-data="{ message: '' }">
  <button class="bg-slate-500 px-4 py-2 text-white" @click="$dispatch('global', { text: 'Hallo andere Komponente' })">
    Komponente benachrichtigen
  </button>
</div>
```

## x-init

Komponenteneigenschaft initialisieren

```html
<!-- Komponenteneigenschaft initialisieren -->
<div class="m-4" x-data="{ count: 0 }" x-init="count = 20">
  <div x-text="'Count: ' + count"></div>
</div>
```

## x-data (2)

Komponenteneigenschaft direkt initialisieren

```html
<!-- Komponenteneigenschaft direkt initialisieren  -->
<div class="p-4" x-data="{ count: 0, init(){ this.count = 10 } }">
  <p x-text="'Count: ' + count"></p>
</div>
```

Komponente mit Methoden

```html
<div
  class="p-4"
  x-data="{ 
    message: 'Lorem ipsum dolor sit amet.', 
    changeText(text){ this.message = text } 
  }"
>
  <button class="bg-slate-500 px-4 py-2 text-white" @click="changeText('Hallo methode')">Ändern</button>
  <p x-text="message"></p>
</div>
```

## x-ref und $refs

Referenz für ein Element setzen und es entfernen

```html
<!-- Referenz für ein Element setzen und es entfernen -->
<div class="p-4" x-data>
  <button class="bg-slate-500 px-4 py-2 text-white" @click="$refs.text.remove()">Entfernen</button>
  <div x-ref="text">
    Lorem ipsum, dolor sit amet consectetur adipisicing elit. Eveniet fuga asperiores quia, adipisci error hic ipsam
    natus, quaerat facere quasi fugit. Adipisci ducimus officia molestias, similique, dolor quod molestiae, placeat
    ipsum est minus tempore accusantium doloribus odio labore totam nulla. Consequuntur facere perspiciatis blanditiis
    explicabo voluptas, repellendus aliquam neque quo!
  </div>
</div>
```

Klassen setzen und entfernen

```html
<div class="space-y-4 p-4" x-data>
  <div class="border border-slate-500 p-4" x-ref="text">
    Lorem ipsum dolor, sit amet consectetur adipisicing elit. Laudantium qui sequi cum delectus aliquid, nam at beatae
    eum laborum dicta.
  </div>
  <!-- Klasse Hinzufügen -->
  <button class="bg-red-400 px-4 py-2 text-white" @click="$refs.text.classList.add('bg-red-500')">
    Rot hinzufügen
  </button>

  <!-- Klasse entfernen -->
  <button class="bg-red-400 px-4 py-2 text-white" @click="$refs.text.classList.remove('bg-red-500')">
    Rot entfernen
  </button>

  <!-- Klasse toggeln -->
  <button class="bg-red-400 px-4 py-2 text-white" @click="$refs.text.classList.toggle('bg-red-500')">
    Rot toggeln
  </button>
</div>
```

## $store und Alpine.store()

Einfacher Speicher

```html
<!-- Einfacher Speicher -->
<div class="p-4" x-data>
  <button class="bg-slate-500 px-4 py-2 text-white" @click="$store.message = 'Hallo Store'">Ändern</button>
  <div x-text="$store.message"></div>
</div>
<script>
  document.addEventListener('alpine:init', () => {
    Alpine.store('message', 'Hallo Welt');
  });
</script>
```

Erweiterter Speicher

```html
<!-- Erweiterter Speicher -->
<div class="p-4" x-data>
  <button class="bg-slate-500 px-4 py-2 text-white" @click="$store.message.changeMessage('Hallo Store')">Ändern</button>
  <div x-text="$store.message.text"></div>
</div>
<script>
  document.addEventListener('alpine:init', () => {
    Alpine.store('message', {
      text: 'Hallo aus dem Speicher',
      changeMessage(text) {
        this.text = text;
      },
    });
  });
</script>
```

## Alpine.bind

Bind Komponente

```html
<!-- Bind Komponente -->
<div class="p-4" x-data="{ open: false }">
  <button class="bg-slate-500 px-4 py-2 text-white" @click="open = true">Anzeigen</button>
  <button x-bind="button('Anzeigen')"></button>
  <div x-show="open">
    Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquid deleniti laborum possimus sunt sit eveniet illo
    officia unde debitis enim qui, necessitatibus, explicabo aspernatur eligendi. Veritatis ab alias nihil qui accusamus
    sint provident, pariatur ratione illo et esse odit repellendus obcaecati? Et dolorum est hic quo debitis repudiandae
    ipsam ullam.
  </div>
</div>
<script>
  document.addEventListener('alpine:init', () => {
    Alpine.bind('button', (label = 'Test') => ({
      class: 'bg-slate-500 px-4 py-2 text-white',
      'x-text'() {
        return label;
      },
      '@click'() {
        this.open = true;
      },
    }));
  });
</script>
```

## Alpine.data()

Data Komponente mit Bind

```html
<!-- Data Komponente mit Bind -->
<div class="p-4" x-data="toggle">
  <button x-bind="button"></button>
  <div x-bind="text"></div>
</div>
<script>
  document.addEventListener('alpine:init', () => {
    Alpine.data('toggle', () => ({
      open: false,
      button: {
        class: 'bg-slate-500 px-4 py-2 text-white',
        '@click'() {
          this.open = !this.open;
        },
        'x-text'() {
          return 'Toggle';
        },
      },
      text: {
        'x-text'() {
          return 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Quia, aspernatur?';
        },
        'x-show'() {
          return this.open;
        },
        '@click.outside'() {
          this.open = false;
        },
      },
    }));
  });
</script>
```

Data Komponente mit einem Intervall

```html
<!-- Data Komponente mit einem Intervall -->
<div class="flex h-screen items-center justify-center" x-data="timer(3)">
  <button
    class="flex size-1/2 items-center justify-center bg-slate-200 text-7xl"
    x-text="count"
    @click="start"
  ></button>
</div>
<script>
  document.addEventListener('alpine:init', () => {
    Alpine.data('timer', (setCount = 5) => ({
      count: 0,
      interval: null,
      init() {
        this.count = setCount;
      },
      start() {
        if (this.interval) {
          clearInterval(this.interval);
        }
        this.interval = setInterval(() => {
          if (this.count > 0) {
            this.count--;
          } else {
            clearInterval(this.interval);
            this.interval = null;
            this.count = setCount;
          }
        }, 1000);
      },
    }));
  });
</script>
```

## $el und $root

Zugriff auf das aktuelle Element

```html
<!-- Zugriff auf das aktuelle Element -->
<div class="p-4" x-data>
  <button @click="$el.innerHTML = 'Hello World'">
    Lorem ipsum dolor sit amet consectetur adipisicing elit. Quia omnis sint tempore optio eveniet expedita a maxime
    esse totam sit ipsum pariatur error ab odit suscipit, dignissimos unde neque. Suscipit necessitatibus architecto
    aspernatur libero, eum officia qui quo perferendis doloribus nostrum praesentium nisi error deserunt rem eius
    ducimus veritatis est!
  </button>
</div>
```

Zugriff auf das Rootelement der Komponente

```html
<!-- Zugriff auf das Rootelement der Komponente -->
<div class="p-4" x-data>
  <button class="bg-slate-400 px-4 py-2 text-white" @click="$root.classList.add('bg-green-400')">Farbe ändern</button>
</div>
```
